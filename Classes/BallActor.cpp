//
//  BallActor.cpp
//  tutorial1
//
//  Created by 小池利幸 on 2017/06/01.
//
//

#include "BallActor.h"
#include "GameScene.h"
USING_NS_CC;

BallActor::BallActor()
:   fieldMin_{},
    fieldMax_{},
    moveDirection_(1.0f, 1.0f),
    moveSpeed_(250.0f),
	startPosition_{},
	prevCollider_(0)
{
    moveDirection_.normalize();
}

bool BallActor::init()
{
    if (!Node::init())
    {
        return false;
    }
    
    // 範囲と初期位置を設定
    auto directorInstance = Director::getInstance();
	CC_ASSERT(directorInstance);
	
    fieldMin_ = directorInstance->getVisibleOrigin();
    fieldMax_ = directorInstance->getWinSize();
	
	startPosition_ = (fieldMax_ - fieldMin_) * 0.5f;
	startPosition_.y = (fieldMax_.y - fieldMin_.y) * 0.4f;
    resetPosition();

    // サークル
	auto radius = 5.f;
	auto drawNode = DrawNode::create();
	CC_ASSERT(drawNode);
	drawNode->drawDot(Vec2::ZERO, radius, Color4F::WHITE);
	addChild(drawNode);
	setContentSize(Size(radius * 2.f, radius * 2.f));

	auto contentHalfSize = getContentSize() * 0.5f;
	fieldMin_ += contentHalfSize;
	fieldMax_ -= contentHalfSize;
	
	// ダグをつける
	setTag(TAG);
	// update を毎フレーム呼ぶ
	scheduleUpdate();
	
    return true;
}

void BallActor::update(float delta)
{
    auto position = getPosition();
    position += moveDirection_ * moveSpeed_ * delta;
    
    // 範囲チェック
	auto reverseSwitchValue = Vec2(1.f, 1.f);
	auto minDiff = position - fieldMin_;
	auto maxDiff = position - fieldMax_;
	
	if (minDiff.x < 0.0f)	position.x -= minDiff.x, reverseSwitchValue.x *= -1.f;
//	if (minDiff.y < 0.0f)	position.y -= minDiff.y, reverseSwitchValue.y *= -1.f;
	if (maxDiff.x > 0.0f)	position.x -= maxDiff.x, reverseSwitchValue.x *= -1.f;
	if (maxDiff.y > 0.0f)	position.y -= maxDiff.y, reverseSwitchValue.y *= -1.f;
	if (minDiff.y < 0.0f)	{
		GameScene* gameScene = (GameScene*)getScene();
		CC_ASSERT(gameScene);
		gameScene->onMiss();
	}
	
	// moveDirection_ *= reverseSwitchValue;
	moveDirection_.x *= reverseSwitchValue.x;
	moveDirection_.y *= reverseSwitchValue.y;
	setPosition(position);
	
	if (reverseSwitchValue.x == -1.f || reverseSwitchValue.y == -1.f)
		prevCollider_ = 0;
}


static float cross(const Vec2& a, const Vec2& b)
{
	return a.x * b.y - a.y * b.x;
}

bool BallActor::attemptBounce(const Node* node)
{
	CC_ASSERT(node);

	auto result = false;

	if (prevCollider_ == (uintptr_t)node)
		return result;
	
	auto opponentSize = node->getContentSize();
	auto opponentExtents = (Vec2)opponentSize * 0.5f;
	auto opponentPosition = node->getPosition();
	
	auto mySize = getContentSize();
	auto myExtents = (Vec2)mySize * 0.5f;
	auto myPosition = getPosition();
	
	auto distance = Vec2(fabsf(myPosition.x - opponentPosition.x), fabsf(myPosition.y - opponentPosition.y)) - myExtents - opponentExtents;
	
	auto isInRectX = distance.x < 0.0f;
	auto isInRectY = distance.y < 0.0f;
	
	// 当たっている?
	result = isInRectX && isInRectY;

	// box vs box を真面目に解決
	if (result) {

		// アルカノイド風ならこんな感じ?
		// auto oppoentNormal = Vec2(opponentPosition - myPosition);	// 対物の方線
		// moveDirection_ = result ? -oppoentNormal.getNormalized() : moveDirection_;
		
		
		
		// boxの一つは点になるように、相手のboxをその分膨らませて考える
		// ボールのみ移動と考える
		// 線分との交点とっての判定でもいいけど、ちょっと計算が長そうなので、もうちょっと簡単に
		// ボールの中心の移動前と移動後が、対象のボックスの対角線上のどの次元なのかを判断して考えるパターンもありそう
		//
		// \   A   / -- a ACとBDを分けるための対角線
		//  +-----+
		//  |\   /|
		//  | \ / |
		// C|  +  |B
		//  | / \ |
		//  |/   \|
		//  +-----+
		// /   D   \ -- b ABとCDを分けるための対角線
		//
		// 移動前・移動後の2点が・・
		// A .. a上 b上
		// B .. a下 b上
		// C .. a上 b下
		// D .. a下 b下
		// E .. a上 bをまたぐ
		// F .. a下 bをまたぐ
		// G .. aをまたぐ b上
		// H .. aをまたぐ b下
		// I .. aをまたぐ bをまたぐ
		// このどれかに決まるはず
		// 反射用の方線は、 中心からの線分で、Iのみ全反射とする
		
		// 方線は
		// A .. ( 0,  1)
		// B .. ( 1,  0)
		// C .. (-1,  0)
		// D .. ( 0, -1)
		
		// 上か下かの判定式は測定点から2点のベクトル(対角)の外積で裏か表かがわかる
		// a × b = |a||b|sinθ .. 2つのベクトルをなすひし形の面積が出てくる。 a b を逆にするとマイナスの面積になる。両方とも単位ベクトルだと、純粋に sinθとなる
		
		// 今回はちょっと手抜きで、 移動後の点のみ、A~Dまでの事象で

		auto extentsA = opponentExtents + myExtents;
		auto extentsB = Vec2(extentsA.x, -extentsA.y);
		auto p0 = myPosition - opponentPosition;
		auto crossA = cross(extentsA, p0);
		auto crossB = cross(extentsB, p0);

		// 方向
		auto vA = (crossA >= 0.0f) ? Vec2(-1.0f,  1.0f) : Vec2( 1.0f, -1.0f);
		auto vB = (crossB >= 0.0f) ? Vec2( 1.0f,  1.0f) : Vec2(-1.0f, -1.0f);
		auto vAB = vA + vB;
		auto normal = vAB.getNormalized();

		// 反射ベクトルを作る
		// 食い込むとフリップのファイティングが起こるが今回は敢えて不完全のまま(本来は逃すことが必要)
		moveDirection_ = moveDirection_ + normal * Vec2::dot(-moveDirection_, normal) * 2.0f;
//		prevCollider_ = static_cast<uintptr_t>(node);
		prevCollider_ = (uintptr_t)node;
	}
	
	return result;
}

void BallActor::resetPosition()
{
	setPosition(startPosition_);
	prevCollider_ = 0;
}
