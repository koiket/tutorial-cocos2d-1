//
//  TitleScene.cpp
//  tutorial1
//
//  Created by 小池利幸 on 2017/05/27.
//
//

#include "TitleScene.h"
#include "ui/CocosGUI.h"
#include "GameScene.h"

USING_NS_CC;

Scene* TitleScene::createScene()
{
    return TitleScene::create();
}

// on "init" you need to initialize your instance
bool TitleScene::init()
{
    //////////////////////////////
    // 1. super init first
    if (!Scene::init())
    {
        return false;
    }

    auto directorInstance = Director::getInstance();
    auto visibleSize = directorInstance->getWinSize();
    auto origin = directorInstance->getVisibleOrigin();
    
    // ラベル
    auto label = Label::createWithSystemFont("BREAKOUT", "", 48);  // createWithTTF はフォントを指定する必要あり
    
    label->setPosition(Vec2(origin.x + visibleSize.width * 0.5f,
                            origin.y + visibleSize.height * 0.5f + label->getContentSize().height));
    this->addChild(label, 1);

    // ボタンを追加
    auto gameButton = ui::Button::create();
    gameButton->setTitleText("→ゲーム");
    gameButton->setTitleFontSize(26);
    gameButton->setPosition(Vec2(origin.x + visibleSize.width * 0.5f,
                                 origin.y + visibleSize.height * 0.5f - gameButton->getContentSize().height));
    gameButton->addTouchEventListener([=](Ref* pSender, ui::Widget::TouchEventType type) {
            if (type == ui::Widget::TouchEventType::ENDED) {
                auto nextScene = GameScene::createScene();
                auto transition = TransitionFade::create(1.0f, nextScene, Color3B::BLACK);
                directorInstance->replaceScene(transition);
            }
        });
    this->addChild(gameButton);
    
    return true;
}
