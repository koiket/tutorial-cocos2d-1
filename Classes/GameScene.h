//
//  GameScene.h
//  tutorial1
//
//  Created by 小池利幸 on 2017/05/28.
//
//

#ifndef GameScene_h
#define GameScene_h

#include "cocos2d.h"

#include "BallActor.h"
#include "PaddleActor.h"

class GameScene : public cocos2d::Scene
{
public:
    GameScene();
    
    static cocos2d::Scene* createScene();
    virtual bool init() override;
    CREATE_FUNC(GameScene);
    
    void onBlockDestroy();
    void onMiss();

protected:
    void changeNextScene(Scene* nextScene);
    
    virtual void update(float delta) override;
    
    int leftBlocks;
    
    BallActor*  ballActor_;
    PaddleActor*    paddleActor_;
    
    void    gameStart();
    void    gameOver();
    
};

#endif /* GameScene_h */
