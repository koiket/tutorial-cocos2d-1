//
//  PaddleActor.h
//  tutorial1
//
//  Created by 小池利幸 on 2017/06/01.
//
//

#ifndef PaddleActor_h
#define PaddleActor_h

class PaddleActor : public cocos2d::Node
{
public:
    PaddleActor();
    
    virtual bool init() override;
    CREATE_FUNC(PaddleActor);
    
    virtual void update(float delta) override;
    
protected:
    cocos2d::Vec2	fieldMin_;
    cocos2d::Vec2	fieldMax_;
    cocos2d::Vec2	moveDirection_;
    float			moveSpeed_;
    
    // virtual void    onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event) override; レイヤークラスではメンバがある
    void            onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
    void            onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
    bool            inputLeft_;
    bool            inputRight_;
};

#endif /* PaddleActor_h */
