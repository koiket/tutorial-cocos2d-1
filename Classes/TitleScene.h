//
//  TitleScene.h
//  tutorial1
//
//  Created by 小池利幸 on 2017/05/27.
//
//

#ifndef TitleScene_h
#define TitleScene_h

#include "cocos2d.h"

class TitleScene : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(TitleScene);
};


#endif /* TitleScene_h */

