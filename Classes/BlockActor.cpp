//
//  BlockActor.cpp
//  tutorial1
//
//  Created by 小池利幸 on 2017/05/28.
//
//

#include "BlockActor.h"
#include "BallActor.h"
#include "GameScene.h"

USING_NS_CC;

bool BlockActor::init()
{
    if (!Node::init())
    {
        return false;
    }
	
	auto box = Rect(-10.f, -5.f, 20.f, 10.f);
	
	auto drawNode = DrawNode::create();
	CC_ASSERT(drawNode);
	drawNode->drawSolidRect(box.origin, box.origin + box.size, Color4F::WHITE);
	addChild(drawNode);
	
	setContentSize(box.size);
	
	this->scheduleUpdate();	// updateを毎フレーム呼び出す
	
    return true;
}

void BlockActor::update(float delta)
{
	//	コリジョンテスト;
	auto gameScene = static_cast<GameScene*>(getScene());
	CC_ASSERT(gameScene);
	auto ballActor = static_cast<BallActor*>(gameScene->getChildByTag(BallActor::TAG));
	auto isContact = false;
	if (ballActor)
		isContact = ballActor->attemptBounce(this);
	// ボールと衝突したら削除
	if (isContact)
		removeFromParent();
}

void BlockActor::onExit()
{
	auto gameScene = static_cast<GameScene*>(getScene());
	CC_ASSERT(gameScene);
	gameScene->onBlockDestroy();	// 削除時にゲームシーンに通知する

	Node::onExit(); // 親のonExitを呼ぶ必要がある
}
