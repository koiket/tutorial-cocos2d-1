//
//  SplashScene.cpp
//  tutorial1
//
//  Created by 小池利幸 on 2017/05/27.
//
//

#include "SplashScene.h"
#include "TitleScene.h"

USING_NS_CC;

Scene* SplashScene::createScene()
{
    return SplashScene::create();
}

// on "init" you need to initialize your instance
bool SplashScene::init()
{
    //////////////////////////////
    // 1. super init first
    if (!Scene::init())
    {
        return false;
    }
    
    auto directorInstance = Director::getInstance();
    auto visibleSize = directorInstance->getWinSize();
    auto origin = directorInstance->getVisibleOrigin();
    
    // ラベル
    auto label = Label::createWithSystemFont("Hinotama Games", "", 48);  // createWithTTF はフォントを指定する必要あり
    
    label->setPosition(Vec2(origin.x + visibleSize.width * 0.5f,
                            origin.y + visibleSize.height * 0.5f + label->getContentSize().height));
    this->addChild(label, 1);
    
    // 時間でタイトルシーンに遷移する
    this->scheduleOnce(schedule_selector(SplashScene::changeNextScene), 3.0f);
    return true;
}

void SplashScene::changeNextScene(float interval)
{
    auto scene = TitleScene::createScene();
    auto fade = TransitionFade::create(0.5f, scene, Color3B::WHITE);
    Director::getInstance()->replaceScene(fade);
}
