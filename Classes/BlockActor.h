//
//  BlockActor.h
//  tutorial1
//
//  Created by 小池利幸 on 2017/05/28.
//
//

#ifndef BlockActor_h
#define BlockActor_h

class BlockActor : public cocos2d::Node
{
public:
	static const int TAG = 2;
	
	virtual bool init() override;
	CREATE_FUNC(BlockActor);

	virtual void onExit() override;
	virtual void update(float delta) override;
};

#endif /* BlockActor_h */
