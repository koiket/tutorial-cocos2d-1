//
//  BallActor.h
//  tutorial1
//
//  Created by 小池利幸 on 2017/06/01.
//
//

#ifndef BallActor_h
#define BallActor_h

class BallActor : public cocos2d::Node
{
public:
	static const int TAG = 1;
	BallActor();

	virtual bool init() override;
    CREATE_FUNC(BallActor);
    
    virtual void update(float delta) override;
	
	// 衝突をテストして跳ね返る
	bool attemptBounce(const Node* node);
	void resetPosition();

	
protected:
	cocos2d::Vec2	fieldMin_;
	cocos2d::Vec2	fieldMax_;
	cocos2d::Vec2	moveDirection_;
	float			moveSpeed_;

	cocos2d::Vec2	startPosition_;
	uintptr_t		prevCollider_;
};

#endif /* BallActor_h */
