//
//  GameScene.cpp
//  tutorial1
//
//  Created by 小池利幸 on 2017/05/28.
//
//

#include "GameScene.h"
#include "ui/CocosGUI.h"
#include "TitleScene.h"

#include "BlockActor.h"

USING_NS_CC;

GameScene::GameScene()  : leftBlocks(0), ballActor_(nullptr), paddleActor_(nullptr)
{
}

Scene* GameScene::createScene()
{
    return GameScene::create();
}

// on "init" you need to initialize your instance
bool GameScene::init()
{
    //////////////////////////////
    // 1. super init first
    if (!Scene::init())
    {
        return false;
    }
    
    auto directorInstance = Director::getInstance();
    auto visibleSize = directorInstance->getWinSize();
    auto origin = directorInstance->getVisibleOrigin();
    
    // ラベル
    auto label = Label::createWithSystemFont("[[Game]]", "", 48);  // createWithTTF はフォントを指定する必要あり
    
    label->setPosition(Vec2(origin.x + visibleSize.width * 0.5f,
                            origin.y + visibleSize.height * 0.5f + label->getContentSize().height));
//    this->addChild(label, 1);
    
    // ボタンが押されたら呼ぶ
    auto titleButton = ui::Button::create();

    titleButton->setTitleText("→タイトル");
    titleButton->setTitleFontSize(26);
    titleButton->setPosition(Vec2(origin.x + visibleSize.width * 0.5f,
                                 origin.y + visibleSize.height * 0.5f - titleButton->getContentSize().height));
    titleButton->addTouchEventListener([=](Ref* pSender, ui::Widget::TouchEventType type) {
        if (type == ui::Widget::TouchEventType::ENDED) {
            auto nextScene = TitleScene::createScene();
            changeNextScene(nextScene);
        }
    });
//    this->addChild(titleButton);
    
    
    // ボールを作成
    ballActor_ = BallActor::create();
	CC_ASSERT(ballActor_);
	ballActor_->unscheduleUpdate();
    addChild(ballActor_);

	// パドルを作成
	paddleActor_ = PaddleActor::create();
	CC_ASSERT(paddleActor_);
	addChild(paddleActor_);

	// ブロックを作成
	auto blockFieldMin = origin;
	auto blockFieldMax = Vec2(visibleSize.width, visibleSize.height);
	blockFieldMin.y = blockFieldMax.y * 0.5f;
	auto blockFieldGap = Vec2(40.0f, 30.0f);
	auto blockFieldSize = blockFieldMax - blockFieldMin - blockFieldGap * 2.0f;
	auto blockNumberX = 14u; // 2以上
	auto blockNumberY = 6u;	// 2以上
	auto blockStepX = blockFieldSize.x / (float)(blockNumberX - 1);
	auto blockStepY = blockFieldSize.y / (float)(blockNumberY - 1);
	
	auto positionOrign = blockFieldMin + blockFieldGap;
	for (auto y = 0u; y < blockNumberY; y++)
		for (auto x = 0u; x < blockNumberX; x++) {
			auto block = BlockActor::create();
			block->setPosition(positionOrign + Vec2(blockStepX * (float)x, blockStepY * (float)y));
			addChild(block);
		}

	leftBlocks = blockNumberY * blockNumberX;
	
	gameStart();
    return true;
}

void GameScene::update(float delta)
{
    //CCLOG("%s: GameScene delta=%f", __FUNCTION__, delta);
}

void GameScene::changeNextScene(Scene* nextScene)
{
    CC_ASSERT(nextScene);
    Director::getInstance()->replaceScene(nextScene);
}

void GameScene::onBlockDestroy()
{
	// ブロックがなくなったらタイトルシーンへ
	if (--leftBlocks <= 0) {
		gameOver();
	}
}

void GameScene::onMiss()
{
	auto directorInstance = Director::getInstance();
	auto visibleSize = directorInstance->getWinSize();
	auto origin = directorInstance->getVisibleOrigin();
	
	// ラベル
	auto label = Label::createWithSystemFont("MISS", "", 48);
	CC_ASSERT(label);
	label->setColor(Color3B::RED);
	label->setPosition(Vec2(origin.x + visibleSize.width * 0.5f, origin.y + visibleSize.height * 0.5f + label->getContentSize().height));
	addChild(label, 1);

	CC_ASSERT(ballActor_);
	ballActor_->unscheduleUpdate();	// ボールを止める
	
	scheduleOnce([label, this](float deltaTime){
		label->removeFromParent();
		gameStart();
	}, 1.0f, std::string(__FUNCTION__));
}


void    GameScene::gameStart()
{
	auto directorInstance = Director::getInstance();
	auto visibleSize = directorInstance->getWinSize();
	auto origin = directorInstance->getVisibleOrigin();
	
	// ラベル
	auto label = Label::createWithSystemFont("START", "", 48);
	CC_ASSERT(label);
	label->setColor(Color3B::GREEN);
	label->setPosition(Vec2(origin.x + visibleSize.width * 0.5f, origin.y + visibleSize.height * 0.5f + label->getContentSize().height));
	addChild(label, 1);

	CC_ASSERT(ballActor_);
	ballActor_->resetPosition();
	
	scheduleOnce([label, this](float deltaTime){
		label->removeFromParent();
		ballActor_->scheduleUpdate();	// ボールを動かす
	}, 1.0f, std::string(__FUNCTION__));
}

void    GameScene::gameOver()
{
	auto directorInstance = Director::getInstance();
	auto visibleSize = directorInstance->getWinSize();
	auto origin = directorInstance->getVisibleOrigin();
	
	// ラベル
	auto label = Label::createWithSystemFont("GAME OVER", "", 48);
	label->setPosition(Vec2(origin.x + visibleSize.width * 0.5f, origin.y + visibleSize.height * 0.5f + label->getContentSize().height));
	label->setColor(Color3B::RED);
	addChild(label, 1);

	CC_ASSERT(ballActor_);
	ballActor_->unscheduleUpdate();	// ボールを止める
	
	scheduleOnce([label, this](float deltaTime){
		CC_ASSERT(label);
		label->removeFromParent();

		auto nextScene = TitleScene::createScene();
		changeNextScene(nextScene);
	}, 1.0f, std::string(__FUNCTION__));
}
