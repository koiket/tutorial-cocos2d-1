//
//  SplashScene.h
//  tutorial1
//
//  Created by 小池利幸 on 2017/05/27.
//
//

// ↓ include ガード
// ↓ http://flast.hateblo.jp/entry/2015/12/05/000000
// ↓ 残念ながら、 pragma one はまだ標準化されてない https://en.wikipedia.org/wiki/Pragma_once
#ifndef SplashScene_h
#define SplashScene_h

#include "cocos2d.h"

class SplashScene : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(SplashScene);

protected:
    void changeNextScene(float interval);
    
};


#endif /* SplashScene_h */

