//
//  PaddleActor.cpp
//  tutorial1
//
//  Created by 小池利幸 on 2017/06/01.
//
//

#include "PaddleActor.h"
#include "BallActor.h"

USING_NS_CC;

PaddleActor::PaddleActor()
:   fieldMin_{},
fieldMax_{},
moveDirection_(0.0f, 0.0f),
moveSpeed_(300.0f),
inputLeft_(false),
inputRight_(false)
{
    moveDirection_.normalize();
}

bool PaddleActor::init()
{
    if (!Node::init())
    {
        return false;
    }
    
    // 範囲と初期位置を設定
    auto directorInstance = Director::getInstance();
	CC_ASSERT(directorInstance);
    fieldMin_ = directorInstance->getVisibleOrigin();
    fieldMax_ = directorInstance->getWinSize();
    setPosition((fieldMax_.x - fieldMin_.x) * 0.5f, (fieldMax_.y - fieldMin_.y) * 0.1f);
    
    // 適当に
	auto box = Rect(-30.f, -5.f, 60.f, 10.f);
	
	auto drawNode = DrawNode::create();
	CC_ASSERT(drawNode);
	drawNode->drawSolidRect(box.origin, box.origin + box.size, Color4F::WHITE);
	addChild(drawNode);
	setContentSize(box.size);


	auto contentHalfSize = getContentSize() * 0.5f;
	fieldMin_ += contentHalfSize;
	fieldMax_ -= contentHalfSize;
	
    // 入力レスポンスのイベントを設定する
    auto listener = EventListenerKeyboard::create();
	listener->onKeyPressed = CC_CALLBACK_2(PaddleActor::onKeyPressed, this);	// CC_CALLBACK_2は2つの引数という意味のようだ
	listener->onKeyReleased = CC_CALLBACK_2(PaddleActor::onKeyReleased, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);	// シーングラフに listener をぶら下げる

	scheduleUpdate();

    return true;
}

void PaddleActor::onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event)
{
	switch (keyCode) {
	case EventKeyboard::KeyCode::KEY_LEFT_ARROW: inputLeft_ = true;	break;
	case EventKeyboard::KeyCode::KEY_RIGHT_ARROW: inputRight_ = true;	break;
	}
}

void PaddleActor::onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event)
{
	switch (keyCode) {
	case EventKeyboard::KeyCode::KEY_LEFT_ARROW: inputLeft_ = false;	break;
	case EventKeyboard::KeyCode::KEY_RIGHT_ARROW: inputRight_ = false;	break;
	}
}

void PaddleActor::update(float delta)
{
	// 移動
	moveDirection_.x = 0.0f;
	moveDirection_.x += inputLeft_ ? -1.0f
						: inputRight_ ? +1.0f
						: 0.0f;
	
    auto position = getPosition();
    position += moveDirection_ * moveSpeed_ * delta;
    
    // 範囲チェック
    auto minDiff = position - fieldMin_;
    auto maxDiff = position - fieldMax_;
	
	position.x = (position.x < fieldMin_.x) ? fieldMin_.x
		: (position.x > fieldMax_.x) ? fieldMax_.x
		: position.x;
	
    setPosition(position);
	
	// 移動結果を元にボールと当たっているか確認
	auto parent = getParent();
	CC_ASSERT(parent);
	auto ballActor = static_cast<BallActor*>(parent->getChildByTag(BallActor::TAG));
	if (ballActor)
		ballActor->attemptBounce(this);
	
}
